/* while($0.scrollWidth>$0.clientWidth){$0.style.fontSize=(parseInt(getComputedStyle($0).fontSize,10)-1)+'px'} */
angular.module('broom', ['ngRoute'])

.factory('db', function () {
  return new Firebase('https://flickering-heat-1121.firebaseio.com/');
})

.config(function ($routeProvider) {
  $routeProvider
    .when('/', {
      templateUrl: '/templates/home.html'
    })
    .when('/room/:room', {
      controller: 'RoomCtrl',
      templateUrl: '/templates/room.html'
    })
    .otherwise('/');
})

.factory('room', function (notify, db, auth, safeApply, session, settings, $location) {
  var offRTJ;
  var rtjNotifications = {};
  var user;
  var haveRoomSettings;
  auth.then(function (_user_) {
    user = _user_;
  });
  var api = {
    set: function (room) {
      haveRoomSettings = false;
      settings.room = room;
      var requestsToJoinRoom;
      var listener = function (snapshot) {
        safeApply(function () {
          var request = snapshot.val();
          var notification = {
            text: request.name + ' wants to join ' + room + '.',
            okText: 'Accept',
            cancelText: 'Reject'
          };
          rtjNotifications[snapshot.key()] = notification;
          notify(notification).then(function () {
            db.child('rooms').child(room).child('users').child(snapshot.key()).set({name: request.name});
            requestsToJoinRoom.child(snapshot.key()).remove();
          }, function () {
            requestsToJoinRoom.child(snapshot.key()).remove();
          });
        });
      };
      var removeListener = function (snapshot) {
        safeApply(function () {
          var notification = rtjNotifications[snapshot.key()];
          if (notification) {
            notification.remove();
          }
        });
      };
      function watchRoom(snapshot) {
        safeApply(function () {
          var value = snapshot.val();
          if (value) {
            if (value.users && value.users[user.uid]) {
              settings.mode = value.users[user.uid].mode;
              haveRoomSettings = true;
            }
            if (value.config && value.config.options) {
              settings.options = value.config.options;
            }
          }
        });
      }
      function onlineWatcher(snapshot) {
        if (snapshot.val()) {
          db.child('rooms').child(room).child('users').child(user.uid).child('online').child(session).onDisconnect().remove();
          db.child('rooms').child(room).child('users').child(user.uid).child('online').child(session).set(true);
        }
      }
      if (offRTJ) {
        offRTJ();
      }
      if (user && room) {
        $location.path('/room/' + room);
        requestsToJoinRoom = db.child('requestsToJoin').child(room);
        requestsToJoinRoom
          .on('child_added', listener);
        requestsToJoinRoom
          .on('child_removed', removeListener);
        offRTJ = function () {
          requestsToJoinRoom
            .off('child_added', listener);
          requestsToJoinRoom
            .off('child_removed', removeListener);
          db.child('rooms').child(room).off('value', watchRoom);
          db.child('rooms').child(room).child('users').child(user.uid).child('online').child(session).remove();
          db.child('.info/connected').off('value', onlineWatcher);
          offRTJ = null;
        };
        db.child('rooms').child(room).on('value', watchRoom);
        db.child('.info/connected').on('value', onlineWatcher);
      } else {
        $location.path('/');
      }
    }
  };
  Object.defineProperty(api, 'haveRoomSettings', {
    get: function () {
      return haveRoomSettings;
    }
  });
  return api;
})

.factory('session', function () {
  function randomDigit() {
    return Math.floor(16 * Math.random()).toString(16);
  }
  var session = '';
  for (var i = 0; i < 31; i++) {
    session += randomDigit();
  }
  return session;
})

.factory('settings', function () {
  return {};
})

.factory('safeApply', function ($rootScope) {
  return function (fn) {
    if ($rootScope.$$phase) {
      fn();
    } else {
      $rootScope.$apply(fn);
    }
  };
})

.controller('SettingsCtrl', function ($scope, auth, db, safeApply, notify, settings, room, $location) {
  $scope.$root.modal = {
    visible: false,
    open: function () {
      this.visible = true;
    }
  };
  auth.then(function (user) {
    var users = db.child('users');
    var userObj = db.child('users').child(user.uid);
    var haveData = false;
    function userChanged(val) {
      safeApply(function () {
        if (!val.exists()) {
          userObj.set({rooms: {}});
        }
        $scope.settings = settings;
        settings.user = val.val();
        $scope.$watch(function () {
          return $location.search().roomName;
        }, function (roomName) {
          if (roomName) {
            settings.roomName = roomName;
          }
        });
        if (settings.user && settings.user.rooms) {
          settings.user.rooms = Object.keys(settings.user.rooms);
          if (!settings.room && settings.user.rooms.indexOf(settings.roomName)  !== -1) {
            settings.room = settings.roomName;
            settings.roomName = '';
          }
        }
        haveData = true;
      });
    }
    userObj.on('value', userChanged);
    $scope.$on('$destroy', function () {
      userObj.off('value', userChanged);
    });
    $scope.$watch('settings.user.name', function (name) {
      if (haveData) {
        userObj.child('name').set(name);
        userObj.child('rooms').once('value', function (snapshot) {
          snapshot.forEach(function (room) {
            db.child('rooms').child(room.key()).child('users').child(user.uid).child('name').set(name);
          });
        });
      }
    });

    $scope.$watch('settings.room', function (_room_) {
      if ($scope.settings) {
        room.set(_room_);
      }
    });

    $scope.$watch('settings.mode', function (mode) {
      if (room.haveRoomSettings && settings.room && mode) {
        db.child('rooms').child(settings.room).child('users').child(user.uid).child('mode').set(mode);
      }
    });

    $scope.$watch('settings.options', function (options) {
      if (room.haveRoomSettings && settings.room && options) {
        db.child('rooms').child(settings.room).child('config/options').set(options + '');
      }
    });

    $scope.$watch('settings.roomName', function (name) {
      if (haveData) {
        settings.roomNameAvailable = undefined;
        if (!name) {
          return;
        }
        db.child('roomNames').child(name)
          .once('value', function (snapshot) {
            safeApply(function () {
              if (name === settings.roomName) {
                settings.roomNameAvailable = !snapshot.exists();
              }
            });
          });
      }
    });

    $scope.requestToJoin = function () {
      if (!settings.roomName) {
        return;
      }
      var roomName = settings.roomName;
      var rtj = db.child('requestsToJoin').child(roomName)
        .child(user.uid);
      rtj.set({
          name: settings.user.name
        });
      function checkForAcceptance(snapshot) {
        var val = snapshot.val();
        if (!val) {
          rtj.off('value', checkForAcceptance);
          db.child('rooms').child(roomName).once('value', function (room) {
            safeApply(function () {
              if (room.exists()) {
                notify({
                  text: 'Your request to join ' + roomName + ' was accepted.',
                  okText: 'Close'
                });
                db.child('users').child(user.uid).child('rooms').child(roomName).set(true);
              } else {
                notify({
                  text: 'Your request to join ' + roomName + ' was rejected.',
                  okText: 'Close'
                });
              }
            });
          }, function () {
            safeApply(function () {
              notify({
                text: 'Your request to join ' + roomName + ' was rejected.',
                okText: 'Close'
              });
            });
          });
        }
      }
      rtj.on('value', checkForAcceptance);
      settings.roomNameAvailable = undefined;
    };

    $scope.createRoom = function () {
      if (!settings.roomName) {
        return;
      }
      var room = {
        users: {
        },
        config: {}
      };
      room.users[user.uid] = {
        online: true,
        away: false,
        name: settings.user.name
      };
      db.child('rooms/' + settings.roomName).set(room);
      db.child('roomNames/' + settings.roomName).set(true);
      userObj.child('rooms').child(settings.roomName).set(true);
    };
  });
})

.factory('User', function (db) {
  function User(props) {
    Object.assign(this, props);
  }
  User.prototype.isMe = function () {
    return this.uid === db.getAuth().uid;
  };
  Object.defineProperty(User.prototype, 'thinking', {
    get: function () {
      return !this.estimate && this.estimate !== 0;
    }
  });
  return User;
})

.controller('RoomCtrl', function ($scope, $routeParams, User, room, db, $location, safeApply) {
  var me;
  db.child('rooms').child($routeParams.room).once('value', function (snapshot) {
    safeApply(function () {
      if (snapshot.exists()) {
        roomExists();
      } else {
        roomNotFound();
      }
    });
  }, function () {
    safeApply(function () {
      roomNotFound();
    });
  });

  function roomExists() {
    $scope.estimates = [0, 0.5, 1, 2, 3, 5, 8, 13, 20, 40, 100, Infinity, '?'];
    room.set($routeParams.room);
    $scope.room = {
      name: $routeParams.room,
    };
    db.child('rooms').child($routeParams.room).child('config/options').on('value', function (snapshot) {
      var val = snapshot.val();
      $scope.room.options = val && val.split ? val.split(/\s+/) : [];
    });
    db.child('rooms').child($routeParams.room).child('users')
      .on('value', function (snapshot) {
        safeApply(function () {
          var users = snapshot.val();
          $scope.room.users = Object.keys(users)
            .map(function (uid) {
              return new User(Object.assign({uid: uid}, users[uid]));
            })
            .filter(function (user) {
              if (user.isMe()) {
                me = user;
              }
              return user.online;
            })
            .sort(function (a, b) {
              var aScore = a.mode === 'estimate' ? 10 : 0;
              aScore += a.isMe() ? 1 : 0;
              var bScore = b.mode === 'estimate' ? 10 : 0;
              bScore += b.isMe() ? 1 : 0;
              if (aScore === bScore) {
                return a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1;
              } else {
                return bScore - aScore;
              }
            });
        });
      });
    Object.defineProperty($scope.room, 'showEstimates', {
      get: function () {
        return this.users && !this.users.filter(function (user) {
          return user.online && user.mode === 'estimate' && !user.estimate && !user.away;
        }).length;
      }
    });
    $scope.$watch(function () {
      return me && me.away;
    }, function (away) {
      if (away === true || away === false) {
        db.child('rooms').child($routeParams.room).child('users').child(db.getAuth().uid).child('away').set(away);
      }
    });
    $scope.$watch(function () {
      return me && me.estimate;
    }, function (estimate) {
      if (me) {
        if (estimate || estimate === 0) {
          db.child('rooms').child($routeParams.room).child('users').child(db.getAuth().uid).child('estimate').set(estimate);
        } else {
          db.child('rooms').child($routeParams.room).child('users').child(db.getAuth().uid).child('estimate').remove();
        }
      }
    });

    $scope.clear = function () {
      var users = db.child('rooms').child($routeParams.room).child('users');
      users.once('value', function (snapshot) {
        safeApply(function () {
          snapshot.forEach(function (user) {
            users.child(user.key()).child('estimate').remove();
          });
        });
      });
    };
  }

  function roomNotFound() {
    $location.url('/?roomName=' + encodeURIComponent($routeParams.room));
  }
})

.factory('notify', function ($q) {
  function notify(config) {
    var deferred = $q.defer();
    config.resolve = deferred.resolve.bind(deferred);
    config.reject = deferred.reject.bind(deferred);
    config.remove = function () {
      var index = notify.notifications.indexOf(config);
      if (index !== -1) {
        notify.notifications.splice(index, 1);
      }
    };
    notify.notifications.unshift(config);
    deferred.promise.finally(config.remove);
    return deferred.promise;
  }
  notify.notifications = [];
  return notify;
})

.directive('notifications', function (notify) {
  return {
    restrict: 'E',
    template: '<notification ng-repeat="notification in notifications" notification="notification">' +
      '</notification>',
    scope: {},
    link: function (scope) {
      scope.notifications = notify.notifications;
    }
  };
})

.directive('notification', function () {
  return {
    restrict: 'E',
    scope: {
      notification: '='
    },
    template: 
      '  <p>{{notification.text}}</p>' +
      '  <button ng-click="notification.resolve()">{{notification.okText}}</button>' +
      '  <button ng-if="notification.cancelText" ng-click="notification.reject()">{{notification.cancelText}}</button>'
  };
})

.directive('fitText', function () {
  return {
    link: function (scope, elm, attrs) {
      var zero = elm[0];

      function fitText() {
        zero.style.fontSize = '';
        while (zero.scrollWidth > zero.clientWidth) {
          zero.style.fontSize = (parseInt(getComputedStyle(zero).fontSize, 10) - 1) + 'px';
        }
        // yucky:
        var text = zero.textContent.trim();
        if (text && text.match(/^[^"'[\]{}\\]+$/i)) {
          var style = document.querySelector('style');
          if (!style) {
            style = document.createElement('style');
            style.setAttribute('type', 'text/css');
            document.querySelector('head').appendChild(style);
          }
          style.appendChild(
            document.createTextNode('.card.back[name="' + text + '"]:before {font-size: ' + zero.style.fontSize + '}')
          );
        }
        // end of yuckiness
      }

      var mo = new MutationObserver(fitText);
      mo.observe(zero, {
        characterData: true,
        childList: true,
        subtree: true
      });

      elm.on('$destroy', function () {
        mo.disconnect();
      });
      fitText();
    }
  };
})

.factory('display', function ($log) {
  var display = [];
  display.error = function (error) {
    $log.error(error);
    this.push({
      type: 'error',
      error: error
    });
  };
  return display;
})

.controller('BroomCtrl', function ($scope, display) {
  $scope.display = display;
})

/*.factory('Room', function (db) {
  function Room() {
  }
  Room.findByName
  return Room;
})*/

.factory('auth', function (db, $q, display) {
  var auth = db.getAuth();
  if (auth) {
    return $q.when(auth);
  } else {
    var deferred = $q.defer();
    db.authAnonymously(function (error, authData) {
      if (error) {
        display.error(error);
        deferred.reject(error);
        return;
      }
      deferred.resolve(authData);
    });
    return deferred.promise;
  }
})

.run(function (db, auth) {
  return false;
  db.set({
    users:{
      dloehr: {
        name: 'David',
        rooms: ['timelords']
      }
    },
    roomNames: ['timelords'],
    requestsToJoin: { // write if !data.exists() || root.rooms[$key].users[$uid]
      timelords: {
        asdfjasjflk: {
          uid: dloehrathome,
          name: 'David'
        }
      }
    },
    rooms: {
      timelords: {
        users: {
          dloehr: true,
          mbeer: {online: true, away: true} // offline
        },
        config: {
          points: [0, 0.5, 1, 2, 3, 5, 8, 13, 20, 40, 100, '?']
        },
        estimates: {
          timelords: [
            {date: '2016-03-18T09:30:00Z', author: 'dloehr', estimate: 3},
            {date: '2016-03-18T09:31:00Z', author: 'dloehr', type: 'clear'},
          ]
        }
      }
    }
  });
})
;

