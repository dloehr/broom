# Broom (for Backlog gROOMing)

## Deploying to Firebase

https://www.firebase.com/docs/hosting/guide/deploying.html

```
npm install -g firebase-tools
firebase login
firebase init --public public
bower install
firebase deploy
```

## Security Rules

You'll also need to configure security rules for your Firebase database.
These rules define which users are allowed to read and write to which
parts of your database.

In the "Security & Rules" section of the Firebase dashboard, paste the
contents of the ```rules.json``` file from this repository.

